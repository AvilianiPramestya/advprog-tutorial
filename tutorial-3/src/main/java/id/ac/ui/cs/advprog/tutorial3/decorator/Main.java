package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.*;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.*;


public class Main {

	public static void main(String[] args) {
		Food crustBeefChickenSpecial = BreadProducer.CRUSTY_SANDWICH.createBreadToBeFilled();

        crustBeefChickenSpecial = FillingDecorator.BEEF_MEAT.addFillingToBread(crustBeefChickenSpecial);

        crustBeefChickenSpecial = FillingDecorator.CHICKEN_MEAT.addFillingToBread(crustBeefChickenSpecial);

        crustBeefChickenSpecial = FillingDecorator.CHILI_SAUCE.addFillingToBread(crustBeefChickenSpecial);

        crustBeefChickenSpecial = FillingDecorator.TOMATO_SAUCE.addFillingToBread(crustBeefChickenSpecial);
        
        crustBeefChickenSpecial = FillingDecorator.CUCUMBER.addFillingToBread(crustBeefChickenSpecial);

        crustBeefChickenSpecial = FillingDecorator.LETTUCE.addFillingToBread(crustBeefChickenSpecial);
        
        crustBeefChickenSpecial = FillingDecorator.CHEESE.addFillingToBread(crustBeefChickenSpecial);
        
        System.out.println("Thank you for your order. This is your food details:");
        System.out.println(crustBeefChickenSpecial.getDescription());
        System.out.println("Your total costs $" + crustBeefChickenSpecial.cost());


	}

}
