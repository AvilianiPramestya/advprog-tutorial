package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.*;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.*;


public class Main {

	public static void main(String[] args) {
		Company company = new Company();
        Ceo ceo = new Ceo("Ani", 200000.0);
        Cto cto = new Cto("Ana", 100000.0);
        BackendProgrammer backendProgrammer = new BackendProgrammer("Ano", 20000.0);
        FrontendProgrammer frontendProgrammer = new FrontendProgrammer("Anu", 30000.0);
        NetworkExpert networkExpert = new NetworkExpert("Ane", 50000.0);
        SecurityExpert securityExpert = new SecurityExpert("Ann", 70000.0);
        UiUxDesigner uiUxDesigner = new UiUxDesigner("Any", 90000.0);

        company.addEmployee(ceo);
        company.addEmployee(cto);
        company.addEmployee(backendProgrammer);
        company.addEmployee(frontendProgrammer);
        company.addEmployee(networkExpert);
        company.addEmployee(securityExpert);
        company.addEmployee(uiUxDesigner);
        
        for (Employees employee : company.getAllEmployees()) {
        	System.out.println("Name: " + employee.getName());
            System.out.println("Role: " + employee.getRole());
            System.out.println("Salary: " + employee.getSalary());
            System.out.println();
        }
        System.out.println("Total Salaries: " + company.getNetSalaries());



	}

}
