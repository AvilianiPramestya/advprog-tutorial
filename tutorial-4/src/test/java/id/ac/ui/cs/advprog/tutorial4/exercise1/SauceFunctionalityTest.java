package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.CarbonaraSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.PlumTomatoSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SauceFunctionalityTest {

    private Sauce carbonara;
    private Sauce marinara;
    private Sauce plumTomato;

    @Before
    public void setUp() throws Exception {
        carbonara = new CarbonaraSauce();
        marinara = new MarinaraSauce();
        plumTomato = new PlumTomatoSauce();

    }

    @Test
    public void testSauceOutput(){
        assertEquals("Carbonara Sauce", carbonara.toString());
        assertEquals("Marinara Sauce", marinara.toString());
        assertEquals("Tomato sauce with plum tomatoes", plumTomato.toString());
    }


}
