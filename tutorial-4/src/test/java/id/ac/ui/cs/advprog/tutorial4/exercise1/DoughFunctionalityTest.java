package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.SuperThickCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThickCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

	public class DoughFunctionalityTest {
	
		private Dough thinCrust;
		private Dough thickCrust;
		private Dough superThickCrust;
		
		@Before
		public void setUp() throws Exception {
			superThickCrust = new SuperThickCrustDough();
			thinCrust= new ThinCrustDough();
			thickCrust= new ThickCrustDough();
			
		}
		
		@Test
		public void testClamsOutput(){
			assertEquals("Bigger with SuperThickCrust",superThickCrust.toString());
			assertEquals("ThickCrust style extra thick crust dough",thickCrust.toString());
			assertEquals("Thin Crust Dough",thinCrust.toString());
	}
	
	

}
