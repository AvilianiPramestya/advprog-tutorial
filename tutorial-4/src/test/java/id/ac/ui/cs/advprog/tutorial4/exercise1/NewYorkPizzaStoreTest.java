package id.ac.ui.cs.advprog.tutorial4.exercise1;


import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.ClamPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.VeggiePizza;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;


public class NewYorkPizzaStoreTest {
	
	private NewYorkPizzaStore newYorkPizzaStore;

    @Before
    public void setUp() {
        newYorkPizzaStore = new NewYorkPizzaStore();
    }

    @Test
    public void createCheesePizzaTest() {
        Pizza pizzaCheese = newYorkPizzaStore.createPizza("cheese");
        assertTrue(pizzaCheese instanceof CheesePizza);
        assertEquals(pizzaCheese.getName(), "New York Style Cheese Pizza");
    }

    @Test
    public void createClamPizzaTest() {
        Pizza pizzaClam = newYorkPizzaStore.createPizza("clam");
        assertTrue(pizzaClam instanceof ClamPizza);
        assertEquals(pizzaClam.getName(), "New York Style Clam Pizza");
    }

    @Test
    public void createVeggiePizzaTest() {
        Pizza pizzaVeggie = newYorkPizzaStore.createPizza("veggie");
        assertTrue(pizzaVeggie instanceof VeggiePizza);
        assertEquals(pizzaVeggie.getName(), "New York Style Veggie Pizza");
    }


}
