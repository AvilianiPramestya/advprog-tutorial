package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.ClamPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.VeggiePizza;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;


public class DepokPizzaStoreTest {
	
	private DepokPizzaStore depokPizzaStore;

    @Before
    public void setUp() {
        depokPizzaStore = new DepokPizzaStore();
    }

    @Test
    public void createCheesePizzaTest()
    {
        Pizza pizzaCheese = depokPizzaStore.createPizza("cheese");
        assertTrue(pizzaCheese instanceof CheesePizza);
        assertEquals(pizzaCheese.getName(), "Depok Style Cheese Pizza");
    }

    @Test
    public void createClamPizzaTest()
    {
        Pizza pizzaClam = depokPizzaStore.createPizza("clam");
        assertTrue(pizzaClam instanceof ClamPizza);
        assertEquals(pizzaClam.getName(), "Depok Style Clam Pizza");
    }

    @Test
    public void createVeggiePizzaTest()
    {
        Pizza pizzaVeggie = depokPizzaStore.createPizza("veggie");
        assertTrue(pizzaVeggie instanceof VeggiePizza);
        assertEquals(pizzaVeggie.getName(), "Depok Style Veggie Pizza");
    }


}
