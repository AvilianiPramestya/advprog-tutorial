package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ManchegoCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.MozzarellaCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ParmesanCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;


public class CheeseFunctionalityTest {
	
    private Cheese manchegoCheese;
    private Cheese mozzarellaCheese;
    private Cheese parmesanCheese;
    private Cheese regiannoCheese;

    @Before
    public void setUp() throws Exception {
        manchegoCheese = new ManchegoCheese();
        mozzarellaCheese = new MozzarellaCheese();
        parmesanCheese = new ParmesanCheese();
        regiannoCheese = new ReggianoCheese();
    }

    @Test
    public void testCheeseOutput(){
        assertEquals("Manchego Cheese",manchegoCheese.toString());
        assertEquals("Shredded Mozzarella",mozzarellaCheese.toString());
        assertEquals("Shredded Parmesan",parmesanCheese.toString());
        assertEquals("Reggiano Cheese",regiannoCheese.toString());
    }


}
