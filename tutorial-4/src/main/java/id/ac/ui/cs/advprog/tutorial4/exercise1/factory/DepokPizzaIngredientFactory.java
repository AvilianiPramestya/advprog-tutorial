package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.SuperThickCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.CarbonaraSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Garlic;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.RedPepper;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Cucumber;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FreshClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.MozzarellaCheese;

public class DepokPizzaIngredientFactory implements PizzaIngredientFactory {

	@Override
	public Dough createDough() {
		return new SuperThickCrustDough();
	}

	@Override
	public Sauce createSauce() {
		return new CarbonaraSauce();
	}

	@Override
	public Cheese createCheese() {
		return  new MozzarellaCheese();
	}

	@Override
	public Veggies[] createVeggies() {
		Veggies[] veggies = {new RedPepper(), new Garlic(), new Cucumber()};
        return veggies;
	}

	@Override
	public Clams createClam() {
		return new FreshClams();
	}

}
