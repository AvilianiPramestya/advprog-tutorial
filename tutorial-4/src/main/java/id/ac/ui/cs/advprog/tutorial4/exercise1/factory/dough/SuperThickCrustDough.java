package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class SuperThickCrustDough implements Dough {
	
	public String toString() {
        return "Bigger with SuperThickCrust";
    }
}
