package applicant;

import static junit.framework.TestCase.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.function.Predicate;

import org.junit.Before;
import org.junit.Test;



public class ApplicantTest {
    // TODO Implement me!
    // Increase code coverage in Applicant class
    // by creating unit test(s)!
    private static Applicant applicant;
    private static Predicate<Applicant> credit;
    private static OutputStream os;

    @Before
    public void setUp() {
        applicant = new Applicant();
        os = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(os);
        System.setOut(ps);
        credit = newApplicant -> newApplicant.getCreditScore() > 600;
    }

    @Test
    public void testCredible() {
        assertEquals(true, applicant.isCredible());
    }

    @Test
    public void testCreditScore() {
        assertEquals(700, applicant.getCreditScore());
    }

    @Test
    public void testEmployementYears() {
        assertEquals(10, applicant.getEmploymentYears());
    }

    @Test
    public void testCrimRecord() {
        assertTrue(applicant.hasCriminalRecord());
    }

    @Test
    public void testEvaluate() {
        Applicant.printEvaluation(Applicant.evaluate(applicant, credit));
        assertEquals("Result of evaluating applicant: accepted\n", os.toString());
    }

}
