import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;


public class ScoreGroupingTest {
    // TODO Implement me!
    // Increase code coverage in ScoreGrouping class
    // by creating unit test(s)!
    private static final Map<String, Integer> scores = new HashMap<>();

    @Before
    public void setUp() {
        scores.put("Alice", 12);
        scores.put("Bob", 15);
        scores.put("Charlie", 11);
        scores.put("Delta", 15);
        scores.put("Emi", 15);
        scores.put("Foxtrot", 11);
    }

    @Test
    public void testGroupScoreBy() {
        Map<Integer, List<String>> group = new HashMap<>();
        group.put(11, Arrays.asList("Charlie", "Foxtrot"));
        group.put(12, Arrays.asList("Alice"));
        group.put(15, Arrays.asList("Emi", "Bob", "Delta"));
        assertEquals(group, ScoreGrouping.groupByScores(scores));
    }
}