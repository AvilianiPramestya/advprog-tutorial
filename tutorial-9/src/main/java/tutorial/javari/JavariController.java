package tutorial.javari;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tutorial.hello.Greeting;
import tutorial.javari.animal.Animal;
import tutorial.javari.animal.Condition;
import tutorial.javari.animal.Gender;


@RestController
public class JavariController {

    private AtomicInteger count = new AtomicInteger();
    private ArrayList<Animal> animals = new ArrayList<>();
    private final Greeting error = new Greeting(404, "Animal not found!");
    private ObjectWriter objectWriter = new ObjectMapper().writerWithDefaultPrettyPrinter();
    private Animal animal;

    @RequestMapping(method = POST, value = "/javari")
    public Animal addAnimal(
            @RequestParam(value = "name", defaultValue = "none") String name,
            @RequestParam(value = "type", defaultValue = "none") String type,
            @RequestParam(value = "gender", defaultValue = "male") String gender,
            @RequestParam(value = "length", defaultValue = "0") String length,
            @RequestParam(value = "weight", defaultValue = "0") String weight,
            @RequestParam(value = "condition", defaultValue = "healthy") String condition
    ) {
        animal = new Animal(
                count.incrementAndGet(), type, name,
                Gender.parseGender(gender), Double.parseDouble(length),
                Double.parseDouble(weight), Condition.parseCondition(condition));
        animals.add(animal);
        return animal;
    }

    @RequestMapping(method = GET, value = "/javari")
    public String showAnimals() throws JsonProcessingException {
        if (animals.size() <= 0) {
            return objectWriter.writeValueAsString("error");
        }
        return objectWriter.writeValueAsString(animals);
    }

    @RequestMapping(method = GET, value = "/javari/{id}")
    public String showAnimals(@PathVariable String id) throws JsonProcessingException {
        animal = findAnimal(Integer.parseInt(id));
        if (animal == null) {
            return objectWriter.writeValueAsString("error");
        }
        return objectWriter.writeValueAsString(animal);
    }

    @RequestMapping(method = DELETE, value = "/javari/{id}")
    public String deleteAnimal(@PathVariable String id) throws JsonProcessingException {
        animal = deleteAnimalWithId(Integer.parseInt(id));
        if (animal == null) {
            return objectWriter.writeValueAsString("error");
        }
        return objectWriter.writeValueAsString(animal);
    }

    private Animal findAnimal(int id) {
        Animal anm = null;
        for (Animal animal : animals) {
            if (animal.getId() == id) {
                anm = animal;
                break;
            }
        }
        return anm;
    }

    private Animal deleteAnimalWithId(int id) {
        for (int i = 0; i < animals.size(); i++) {
            if (animals.get(i).getId() == id) {
                return animals.remove(i);
            }
        }
        return null;
    }
}
