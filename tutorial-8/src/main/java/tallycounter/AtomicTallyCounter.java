package tallycounter;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicTallyCounter extends TallyCounter {

    private AtomicInteger atomicCounter = new AtomicInteger(0);

    public void increment() {
        atomicCounter.getAndIncrement();
    }

    public void decrement() {
        atomicCounter.getAndDecrement();
    }

    public int value() {
        return atomicCounter.intValue();
    }
}
