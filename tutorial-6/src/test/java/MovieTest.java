import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MovieTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable

    private Movie movie1, movie2;

    @Before
    public void setUp() {
        movie1 = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        movie2 = new Movie("The Avengers", Movie.REGULAR);
    }

    @Test
    public void getTitle() {
        assertEquals("Who Killed Captain Alex?", movie1.getTitle());
    }

    @Test
    public void setTitle() {
        movie1.setTitle("Bad Black");

        assertEquals("Bad Black", movie1.getTitle());
    }

    @Test
    public void getPriceCode() {
        assertEquals(Movie.REGULAR, movie1.getPriceCode());
    }

    @Test
    public void setPriceCode() {
        movie1.setPriceCode(Movie.CHILDREN);

        assertEquals(Movie.CHILDREN, movie1.getPriceCode());
    }

    @Test
    public void equals() {
        assertEquals(movie1.equals(movie2), false);
        assertFalse(movie1.hashCode() == movie2.hashCode());
        assertFalse(movie1.equals(null));
    }
}