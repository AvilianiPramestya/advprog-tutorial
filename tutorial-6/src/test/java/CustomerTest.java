import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CustomerTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable

    private Customer customer;
    private Movie movie1, movie2;
    private Rental rental1, rental2;

    @Before
    public void setUp() {
        customer = new Customer("Alice");
        movie1 = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        movie2 = new Movie("Train to Busan", Movie.REGULAR);
        rental1 = new Rental(movie1, 3);
        rental2 = new Rental(movie2, 3);

    }

    @Test
    public void getName() {
        assertEquals("Alice", customer.getName());
    }

    @Test
    public void statementWithSingleMovie() {
        customer.addRental(rental1);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    @Test
    public void statementWithMultipleMovies() {
        customer.addRental(rental1);
        customer.addRental(rental2);

        String res = customer.statement();
        String[] line = res.split("\n");

        assertEquals(5, line.length);
        assertTrue(res.contains("Amount owed is 7.0"));
        assertTrue(res.contains("2 frequent renter points"));
    }

    @Test
    public void htmlstatementWithSingleMovie() {
        customer.addRental(rental1);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
    }

    @Test
    public void htmlstatementWithMultipleMovies() {
        customer.addRental(rental1);
        customer.addRental(rental2);

        String res = customer.statement();
        String[] line = res.split("\n");

        assertEquals(5, line.length);
    }


}