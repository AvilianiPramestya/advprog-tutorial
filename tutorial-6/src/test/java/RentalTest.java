import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class RentalTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable

    private Movie movie, movie2, movie3;
    private Rental rent, rental2, rental3;

    @Before
    public void setUp() {
        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        movie2 = new Movie("Insidious", Movie.NEW_RELEASE);
        movie3 = new Movie("Zootopia", Movie.CHILDREN);

        rent = new Rental(movie, 3);
        rental2 = new Rental(movie2, 3);
        rental3 = new Rental(movie3, 4);
    }

    @Test
    public void getMovie() {
        assertEquals(movie, rent.getMovie());
    }

    @Test
    public void getDaysRented() {
        assertEquals(3, rent.getDaysRented());
    }

    @Test
    public void testMoviePrice() {
        assertEquals("" + rent.getRentalPrice(), "3.5");
        assertEquals("9.0", "" + rental2.getRentalPrice());
        assertEquals("3.0", "" + rental3.getRentalPrice());
    }

    @Test
    public void frequentRenterPonts() {
        assertEquals(2, rental2.getFrequentRenterPoint());
        assertEquals(1, rent.getFrequentRenterPoint());
    }
}