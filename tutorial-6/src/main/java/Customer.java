import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class Customer {

    private String name;
    private List<Rental> rentals = new ArrayList<>();

    public Customer(String name) {
        this.name = name;
    }

    public void addRental(Rental arg) {
        rentals.add(arg);
    }

    public String getName() {
        return name;
    }

    public String statement() {
        Iterator<Rental> iterator = rentals.iterator();
        String result = "Rental Record for " + getName() + "\n";

        while (iterator.hasNext()) {
            Rental each = iterator.next();

            // Show figures for this   rental
            result += "\t" + each.getMovie().getTitle() + "\t"
                    + String.valueOf(each.getRentalPrice()) + "\n";
        }

        // Add footer lines
        result += "Amount owed is " + String.valueOf(getMovieCharge()) + "\n";
        result += "You earned " + String.valueOf(frequentRenterPoints())
                + " frequent renter points";

        return result;
    }

    private double getMovieCharge() {
        double temp = 0;
        Iterator<Rental> itr = rentals.iterator();
        while (itr.hasNext()) {
            Rental each = itr.next();
            temp += each.getRentalPrice();
        }
        return temp;
    }

    private int frequentRenterPoints() {
        int temp = 0;
        Iterator<Rental> itr = rentals.iterator();
        while ((itr.hasNext())) {
            Rental each = itr.next();
            temp += each.getFrequentRenterPoint();
        }
        return temp;
    }

    public String htmlStatement() {
        Iterator<Rental> iterator = rentals.iterator();
        String result = "<h1>Rental Record for <em> "
                + getName() + "</em></h1>\n";

        while (iterator.hasNext()) {
            Rental each = iterator.next();

            // Show figures for this rental
            result += each.getMovie().getTitle()
                    + ":" + String.valueOf(each.getRentalPrice()) + "<br>\n";
        }

        // Add footer lines
        result += "<p>Amount owed is <em>" + String.valueOf(getMovieCharge()) + "</em></p>\n";
        result += "<p>You earned <em>" + String.valueOf(frequentRenterPoints())
                + " frequent renter points</em></p>";

        return result;
    }
}